<?php
/*------- creating exployee class --------*/
//require_once('./db_class/config.php');
require_once('./db_class/config.php');
class Employee{
	/*
		$_config protedted variable contains all configuration array
	*/
	protected $_config;
    /*
		$dbc is contains pdo instance 
    */
    public $dbc;
	/***
	Create Construct method
	__construct() method sets value of $_config variable and
	eshtablish a pdo connection by calling getPDOConnection()
	***/
    public function __construct( array $config ) {
        $this->_config = $config;
        $this->getPDOConnection();
    }
    /*
	it's a __destruct() method
	it disconnects of the eshtablished connection by calling __destruct() method
    */
    public function __destruct(){
    	$this->dbc = NULL;
    }
    /*
	it's a method which makes a db connection and it called from __construct() method
    */
    public function getPDOConnection(){
    	if($this->dbc == NULL){
            $dsn = "" .
                $this->_config['driver'] .
                ":host=" . $this->_config['host'] .
                ";dbname=" . $this->_config['dbname'];
            try {
                $this->dbc = new PDO( $dsn, $this->_config[ 'username' ], $this->_config[ 'password' ] );
                $this->dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch( PDOException $e ) {
                echo __LINE__.$e->getMessage();
            }
    	}
    }
    /*
	it's a method which creates the new item
    */
    public function insert_new( $sql ) {
        try {
        	$this->dbc->query($sql);
        	return $this->dbc->lastInsertId();
        } catch(PDOException $e) {
        	echo __LINE__.$e->getMessage();
        }
    }
    public function get_list(){
    	try {
		    $this->dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $this->dbc->prepare("SELECT * FROM employee"); 
		    $stmt->execute();
		    // set the resulting array to associative
		    $result = $stmt->fetchall(PDO::FETCH_ASSOC);
		    return $result;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
    }



}