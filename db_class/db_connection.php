<?php
/*
	making database connection
*/
require_once('config.php');
class Db_connection{
	protected $_config;
    // Database Connection
    public $dbc;
    /*** Create Construct method ***/
    public function __construct( array $config ) {
        $this->_config = $config;
        $this->getPDOConnection();
    }
    /*** Create Destruct method ***/
    public function __destruct(){
    	$this->dbc = NULL;
    }
    public function getPDOConnection(){
    	if($this->dbc == NULL){
    		// Create the connection
            $dsn = "" .
                $this->_config['driver'] .
                ":host=" . $this->_config['host'] .
                ";dbname=" . $this->_config['dbname'];
            try {
                $this->dbc = new PDO( $dsn, $this->_config[ 'username' ], $this->_config[ 'password' ] );
                $this->dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch( PDOException $e ) {
                echo __LINE__.$e->getMessage();
            }
    	}
    }

    public function insert_new( $sql ) {
        try {
        	$this->dbc->query($sql);
        	return $this->dbc->lastInsertId();
        } catch(PDOException $e) {
        	echo __LINE__.$e->getMessage();
        }
    }
    public function getQuery( $sql ) {
		$stmt = $this->dbc->query( $sql );
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
		return $stmt;
	}
}
//$con = new Db_connection($localhost);