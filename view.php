<?php require_once('template/header.php');?>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="page-header">Header Area</h2>
					</div>
				</div>
			</div>
		</header>
		<section>
			<div class="container">
				<div class="row">
				<div class="col-sm-3">
					<ul class="list-group">
						<li class="list-group-item"><a href="index.php">Employee List</a></li>
						<li class="list-group-item"><a href="create.php">Create New</a></li>
					</ul>
				</div>
				<div class="col-sm-9">
					<h3>Side Menu</h3>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Panel title</h3>
						</div>
						<div class="panel-body">
							Panel content
							<a href="index.php" class="btn btn-default">Back</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>
<?php require_once('template/footer.php'); ?>