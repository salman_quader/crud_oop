<?php require_once('template/header.php');?>
<?php
	try {
		$emp_ob->dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if(isset($_POST['emp_name']) && isset($_POST['emp_id']) && isset($_POST['phone'])){
			$data['emp_name'] = $_POST['emp_name'];
			$data['emp_id'] = $_POST['emp_id'];
			$data['phone'] = $_POST['phone'];
		}else{
			//header('Location:index.php');
		}
		if(!empty($data)){
			$stmt = $emp_ob->dbc->prepare("INSERT INTO employee (emp_name, emp_id, designation_id, phone)VALUES ('".$data['emp_name']."', '".$data['emp_id']."', '0', '".$data['phone']."')"); 
		    $stmt->execute();
		    if($emp_ob->dbc->lastInsertId()){
		    	header('Location:index.php');
		    }else{
		    	header('Location:create.php','refresh');
		    }
		}
	}
	catch(PDOException $e) {
	    echo "Error: " . $e->getMessage();
	}
?>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="page-header">Header Area</h2>
					</div>
				</div>
			</div>
		</header>
		<section>
			<div class="container">
				<div class="row">
				<div class="col-sm-3">
					<h3>Side Menu</h3>
					<ul class="list-group">
						<li class="list-group-item"><a href="index.php">Employee List</a></li>
						<li class="list-group-item"><a href="create.php">Create New</a></li>
					</ul>
				</div>
				<div class="col-sm-9">
					<h3>Side Menu</h3>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Create New Employee</h3>
						</div>
						<div class="panel-body">
							<form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
								<div class="form-group">
									<label for="emp_name">Employee name:</label>
									<input type="text" class="form-control" name="emp_name" id="emp_name">
								</div>
								<div class="form-group">
									<label for="emp_id">Employee ID:</label>
									<input type="text" class="form-control" name="emp_id" id="emp_id">
								</div>
								<div class="form-group">
									<label for="phone">Phone:</label>
									<input type="text" class="form-control" name="phone" id="phone">
								</div>
								<button type="submit" class="btn btn-default">Submit</button>
								<a href="index.php" class="btn btn-default">Back</a>
							</form>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>
<?php require_once('template/footer.php'); ?>