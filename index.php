<?php require_once('template/header.php');?>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="page-header">Header Area</h2>
					</div>
				</div>
			</div>
		</header>
		<section>
			<div class="container">
				<div class="row">
				<div class="col-sm-3">
					<h3>Side Menu</h3>
					<ul class="list-group">
						<li class="list-group-item"><a href="<?=$_SERVER['PHP_SELF']?>">Employee List</a></li>
						<li class="list-group-item"><a href="create.php">Create New</a></li>
					</ul>
				</div>
				<div class="col-sm-9">
					<h3>Employee List</h3>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">List</h3>
						</div>
						<div class="panel-body">
							<?php
								$result = $emp_ob->get_list();
								if(!empty($result)){
							?>
								<table class="table table-stripped table-hover">
									<thead>
										<tr>
											<th>Employee Name</th>
											<th>Employee ID</th>
											<th>Phone</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($result as $val):?>
										<tr>
											<td><?=$val['emp_name']?></td>
											<td><?=$val['emp_id']?></td>
											<td><?=$val['phone']?></td>
											<td>
												<a href="edit.php" class="btn btn-default">Edit</a>
												<a href="view.php" class="btn btn-default">View</a>
												<a href="delete.php" class="btn btn-default">Delete</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							<?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>
<?php require_once('template/footer.php'); ?>