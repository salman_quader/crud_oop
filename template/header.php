<?php
	require_once('all_classes/employee.php');
	/*
	creating object of employee class
	*/
	$emp_ob = new Employee($localhost);
	// $sql = "INSERT INTO employee (emp_name, emp_id, designation_id, phone)VALUES ('John', 'john_101', 'Web Developer', '01774665384')";
	// if ($emp_ob->insert_new($sql)) {
	//     echo "New record created successfully";
	// } else {
	//     echo $emp_ob->dbc->error;
	// }
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Crud using oop</title>
		<!-- Bootstrap -->
		<link href="assets/bootstrap_3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">
	</head>